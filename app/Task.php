<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Task extends Model
{
    protected $fillable = [
        'name', 'description'
    ];
    
    public function files()
    {
        return $this->belongsToMany('App\File');
    }

    public function creator()
    {
        return $this->belongsTo('App\User','user_id');
    }

    public function getFileDownload($id)
    {
        $file = DB::table('tasks')
        ->join('attachments','tasks.id','=','attachments.attachment_id')
        ->select('attachments.name')
        ->where('tasks.id','=',$id)
        ->where('attachments.type','tasks')
        ->get();
        return $file;
    }

}

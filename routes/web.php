<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes([
  'register' => false, // Registration Routes...
  'reset' => false, // Password Reset Routes...
  'verify' => false, // Email Verification Routes...
]);

Route::group(['middleware' => ['auth']], function() {
    Route::resource('roles','RoleController');
    Route::resource('users','UserController');
    Route::resource('projects','ProjectController');
    Route::resource('tasks','TaskController');
    Route::resource('comments','CommentController');
    Route::resource('logworks','LogworkController');
    
    ROute::get('changestatusproject/{id}/{pid}','ProjectController@changeStatus');
    Route::get('logworkremoved/{id}','LogworkController@is_removed');
    Route::get('download-logworks/{name}','LogworkController@download');
    Route::get('commentremoved/{id}','CommentController@is_removed');
    Route::get('download-comments/{name}','CommentController@download');
    Route::get('create-task/{id}','TaskController@createTicket');
    Route::get('task/{id}','TaskController@index');
    Route::get('download-attachemnt/{name}','TaskController@download');
});

Route::get('/home', 'HomeController@index')->name('home');
Route::resource('yourwork','YourworkController');
Route::get('projectExcel','ProjectController@excel');
Route::get('userBlock/{id}','UserController@userBlock');
Route::get('editUser/{id}','UserController@editUser');
Route::put('updateUser/{id}','UserController@updateUser');
Route::get('changePassword/{id}','UserController@changePassword');
Route::put('updatePassword/{id}','UserController@updatePassword');
Route::resource('logs','LogController');
Route::get('download/{filename}', 'LogController@getDownload');
// LocalizationController
Route::get('lang/{locale}', 'LocalizationController@index');
// ROUTE for getImage from photos folder
Route::get('/photos/{filename}', function ($filename)
{
    $path = storage_path() . '/photos/' . $filename;
    if(!File::exists($path)) abort(404);
    $file = File::get($path);
    $type = File::mimeType($path);
    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);
    return $response;
})->name('photo');
